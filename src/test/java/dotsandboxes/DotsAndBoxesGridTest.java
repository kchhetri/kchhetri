package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    // FIXME: You need to write tests for the two known bugs in the code.

    @Test
    public void testBoxCompletion() {
        logger.info("Testing box completion");

        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2);

        // Create the grid to have a whole box.
        grid.drawHorizontal(0, 0, 1); // Top line
        grid.drawVertical(0, 0, 1);   // Left line
        grid.drawHorizontal(0, 1, 1); // Bottom line
        grid.drawVertical(1, 0, 1);   // Right line

        // Check that the box is complete
        assertTrue(grid.boxComplete(0, 0));
    }

    @Test
    public void testDrawingAlreadyDrawnHorizontalLine() {

        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2);

        logger.info("Testing drawing an already drawn horizontal line");

        // Draw a horizontal line
        grid.drawHorizontal(0, 0, 1);

        // Try drawing the same line again expecting an exception
        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> {
            grid.drawHorizontal(0, 0, 1);
        });
        assertEquals("Line has already been drawn", exception.getMessage());
    }

    @Test
    public void testDrawingAlreadyDrawnVerticalLine() {

        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2);

        logger.info("Testing drawing an already drawn vertical line");

        // Draw a vertical line
        grid.drawVertical(0, 0, 1);

        // Try drawing the same line again expecting an exception
        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> {
            grid.drawVertical(0, 0, 1);
        });
        assertEquals("Line has already been drawn", exception.getMessage());
    }

}
